import requests
from requests_aws4auth import AWS4Auth
import json
import datetime

bp_list = [
    {'data_timestamp': 0.0, 'diastolic': 120, 'systolic': 140, 'pulse_rate': 72},
    {'data_timestamp': 0.0, 'diastolic': 121, 'systolic': 141, 'pulse_rate': 73},
    {'data_timestamp': 0.0, 'diastolic': 122, 'systolic': 142, 'pulse_rate': 74},
    {'data_timestamp': 0.0, 'diastolic': 123, 'systolic': 143, 'pulse_rate': 75},
    {'data_timestamp': 0.0, 'diastolic': 124, 'systolic': 144, 'pulse_rate': 76},
]


def test_07_createBloodPressure_multiple_pass(login):
    # put for loop
    for bp in bp_list:

        query = """
        
        mutation createBloodPressure($CreateBloodPressureInput:CreateBloodPressureInput!){
            createBloodPressure(input:$CreateBloodPressureInput){
                id
                data_timestamp
                diastolic
                systolic
                pulse_rate
            }
        }

        """
        ct = datetime.datetime.utcnow()
        timestamp = ct.timestamp()
        bp['data_timestamp'] = timestamp

        variable = """
        {
        "CreateBloodPressureInput": {
            "data_timestamp":""" + str(timestamp) + """,
            "diastolic": """ + str(bp['diastolic'])+""",
            "systolic": """ + str(bp['systolic'])+""",
            "pulse_rate" : """ + str(bp['pulse_rate'])+"""
        }
        }
        """

        headers = {'x-api-key': login['X_API_KEY'],
                   'Authorization': login['Authorization'],
                   'Content-Type': "application/graphql"}

        response = requests.post(login['APPSYNC_API_ENDPOINT_URL'], json={
            'query': query, 'variables': variable}, headers=headers)

        print(response.json())
        print(response)
        data = response.json()['data']
        assert data['createBloodPressure']['data_timestamp'] == bp['data_timestamp']
        assert data['createBloodPressure']['diastolic'] == bp['diastolic']
        assert data['createBloodPressure']['systolic'] == bp['systolic']
        assert data['createBloodPressure']['pulse_rate'] == bp['pulse_rate']

    # clean up
    for bp in bp_list:
        query = """
        mutation deleteBloodPressure($DeleteBloodPressureInput:DeleteBloodPressureInput!){
        deleteBloodPressure(input:$DeleteBloodPressureInput){
                id
                data_timestamp
                diastolic
                systolic
                pulse_rate
            }
        }
        """
        variable = """
        {
            "DeleteBloodPressureInput": {
                "data_timestamp": """ + str(bp['data_timestamp']) + """
            }
        }
        """
        headers = {'x-api-key': login['X_API_KEY'],
                   'Authorization': login['Authorization'],
                   'Content-Type': "application/graphql"}

        response = requests.post(login['APPSYNC_API_ENDPOINT_URL'], json={
            'query': query, 'variables': variable}, headers=headers)
