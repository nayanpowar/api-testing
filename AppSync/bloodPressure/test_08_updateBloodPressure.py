import requests
from requests_aws4auth import AWS4Auth
import json

def test_08_updateBloodPressure(login, blood_pressure):
    query = """
      mutation updateBloodPressure($UpdateBloodPressureInput:UpdateBloodPressureInput!){
        updateBloodPressure(input:$UpdateBloodPressureInput){
            id
            data_timestamp
            diastolic
            systolic
            pulse_rate
        }
    }
    
    """
    timestamp = blood_pressure['data_timestamp']
    variable = """
    {
      "UpdateBloodPressureInput": {
        "data_timestamp":"""+ str(timestamp)+""",
        "diastolic": 124,
        "systolic": 147,
        "pulse_rate" : 81
      }
    }
    """

    headers = {'x-api-key': login['X_API_KEY'],
            'Authorization': login['Authorization'],
            'Content-Type': "application/graphql"}

    response = requests.post(login['APPSYNC_API_ENDPOINT_URL'], json={
    'query': query, 'variables': variable}, headers=headers)
    print(response.json())
    print(response)

    data = response.json()['data']
    assert data['updateBloodPressure']['data_timestamp'] == blood_pressure['data_timestamp'] 
    assert data['updateBloodPressure']['diastolic'] == 124
    assert data['updateBloodPressure']['systolic'] == 147
    assert data['updateBloodPressure']['pulse_rate'] == 81
    query = """
    query getBloodPressure($data_timestamp: Float!){
        getBloodPressure(data_timestamp: $data_timestamp){
            id
            data_timestamp
            diastolic
            systolic
            pulse_rate
        }
    }
    
    """
    variable = """
    {
        "data_timestamp":"""+ str(timestamp)+"""

    }
    """

    response = requests.post(login['APPSYNC_API_ENDPOINT_URL'], json={
    'query': query, 'variables': variable}, headers=headers)
    
    data = response.json()['data']
    assert data['getBloodPressure']['data_timestamp'] == blood_pressure['data_timestamp'] 
    assert data['getBloodPressure']['diastolic'] == 124
    assert data['getBloodPressure']['systolic'] == 147
    assert data['getBloodPressure']['pulse_rate'] == 81
    print(response.json())
    print(response)
    
    