import requests
from requests_aws4auth import AWS4Auth

def test_09_getBloodPressure(login, blood_pressure):
    query = """
    query getBloodPressure($data_timestamp: Float!){
        getBloodPressure(data_timestamp: $data_timestamp){
            id
            data_timestamp
            diastolic
            systolic
            pulse_rate
        }
    }
    
    """
    timestamp = blood_pressure['data_timestamp']
    variable = """
    {
        "data_timestamp":"""+ str(timestamp)+"""

    }
    """

    headers = {'x-api-key': login['X_API_KEY'],
            'Authorization': login['Authorization'],
            'Content-Type': "application/graphql"}

    response = requests.post(login['APPSYNC_API_ENDPOINT_URL'], json={
    'query': query, 'variables': variable}, headers=headers)
    
    data = response.json()['data']
    assert data['getBloodPressure']['data_timestamp'] == blood_pressure['data_timestamp'] 
    assert data['getBloodPressure']['diastolic'] == blood_pressure['diastolic']
    assert data['getBloodPressure']['systolic'] == blood_pressure['systolic']
    assert data['getBloodPressure']['pulse_rate'] == blood_pressure['pulse_rate']
    print(response.json())
    print(response)