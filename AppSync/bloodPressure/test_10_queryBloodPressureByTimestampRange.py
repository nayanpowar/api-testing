import requests
from requests_aws4auth import AWS4Auth
import json
import datetime

def cr_bp(login,count):
    data_list = []
    for i in range(count):
        query = """
        mutation createBloodPressure($CreateBloodPressureInput:CreateBloodPressureInput!){
            createBloodPressure(input:$CreateBloodPressureInput){
                id
                data_timestamp
                diastolic
                systolic
                pulse_rate
            }
        }

        """
        ct = datetime.datetime.utcnow()
        timestamp = ct.timestamp()
        
        variable = """
        {
        "CreateBloodPressureInput": {
            "data_timestamp":""" + str(timestamp) +""",
            "diastolic": 123,
            "systolic": 145,
            "pulse_rate" : 80
        }
        }
        """

        headers = {'x-api-key': login['X_API_KEY'],
                'Authorization': login['Authorization'],
                'Content-Type': "application/graphql"}

        response = requests.post(login['APPSYNC_API_ENDPOINT_URL'], json={
        'query': query, 'variables': variable}, headers=headers)
        
        data = response.json()['data']
        data_list.append(data['createBloodPressure'])

    return data_list

    
def test_10_queryBloodPressureByTimestampRange_pass(login, blood_pressure):
    data_list = cr_bp(login, 3)
    print(data_list)
    query = """
    query queryBloodPressureByTimestampRange ($from_ts: Float, $to_ts: Float) {
    queryBloodPressureByTimestampRange(from_ts:$from_ts, to_ts:$to_ts) {
    items {
                id
                data_timestamp
                diastolic
                systolic
                pulse_rate
            }
        }
    }
    """
    ct = datetime.datetime.utcnow()
    timestamp = ct.timestamp()

    variable = """
    {
        "from_ts": """ + str(data_list[0]['data_timestamp']) + """,
        "to_ts": """ + str(data_list[2]['data_timestamp']) + """
    }

    """
    headers = {'x-api-key': login['X_API_KEY'],
            'Authorization': login['Authorization'],
            'Content-Type': "application/graphql"}

    response = requests.post(login['APPSYNC_API_ENDPOINT_URL'], json={
    'query': query, 'variables': variable}, headers=headers)
    

    items_ = response.json()['data']['queryBloodPressureByTimestampRange']['items']
    items = sorted(items_, key=lambda d: d['data_timestamp']) 

    print ('length of items is:',len(items))
    assert len(items)  == 3
    for i, entry in enumerate(items):
        assert entry['data_timestamp'] == data_list[i]['data_timestamp']
        assert entry['diastolic'] == data_list[i]['diastolic']
        assert entry['systolic'] == data_list[i]['systolic']
        assert entry['pulse_rate'] == data_list[i]['pulse_rate']

    print(response.json())
    print(response)
    # assert False
    
    # end of query blood pressure by time stamp
    
    # clean up
    
    query = """
    mutation deleteBloodPressure($DeleteBloodPressureInput:DeleteBloodPressureInput!){
        deleteBloodPressure(input:$DeleteBloodPressureInput){
                id
                data_timestamp
                diastolic
                systolic
                pulse_rate
            }
        }
        """  
      
   
    # print (data_list)
    # assert False
    for x in data_list:
        variable = """
            {
                "DeleteBloodPressureInput": {
                    "data_timestamp": """+ str(x['data_timestamp']) +"""
                }
            }
            """
        headers = {'x-api-key': login['X_API_KEY'],
                    'Authorization': login['Authorization'],
                    'Content-Type': "application/graphql"}

        response = requests.post(login['APPSYNC_API_ENDPOINT_URL'], json={
                'query': query, 'variables': variable}, headers=headers)
            
        print('attempte to delete: ', x['data_timestamp'])
        assert response.status_code == 200
        