import requests
from requests_aws4auth import AWS4Auth
import json
import datetime

def test_06_createBloodPressure_pass(login):
    query = """
    mutation createBloodPressure($CreateBloodPressureInput:CreateBloodPressureInput!){
        createBloodPressure(input:$CreateBloodPressureInput){
            id
            data_timestamp
            diastolic
            systolic
            pulse_rate
        }
    }

    """
    ct = datetime.datetime.utcnow()
    timestamp = ct.timestamp()
    
    variable = """
    {
    "CreateBloodPressureInput": {
        "data_timestamp":""" + str(timestamp) +""",
        "diastolic": 123,
        "systolic": 145,
        "pulse_rate" : 80
    }
    }
    """

    headers = {'x-api-key': login['X_API_KEY'],
            'Authorization': login['Authorization'],
            'Content-Type': "application/graphql"}

    response = requests.post(login['APPSYNC_API_ENDPOINT_URL'], json={
    'query': query, 'variables': variable}, headers=headers)
    
    data = response.json()['data']
    assert data['createBloodPressure']['data_timestamp'] == timestamp
    assert data['createBloodPressure']['diastolic'] == 123
    assert data['createBloodPressure']['systolic'] == 145
    assert data['createBloodPressure']['pulse_rate'] == 80
    
    
    print(response.json())
    print(response)
    assert False
    
    # clean up
    query = """
    mutation deleteBloodPressure($DeleteBloodPressureInput:DeleteBloodPressureInput!){
    deleteBloodPressure(input:$DeleteBloodPressureInput){
            id
            data_timestamp
            diastolic
            systolic
            pulse_rate
        }
    }
    """
    variable = """
    {
        "DeleteBloodPressureInput": {
            "data_timestamp": """+ str(timestamp) +"""
        }
    }
    """
    headers = {'x-api-key': login['X_API_KEY'],
            'Authorization': login['Authorization'],
            'Content-Type': "application/graphql"}

    response = requests.post(login['APPSYNC_API_ENDPOINT_URL'], json={
        'query': query, 'variables': variable}, headers=headers)