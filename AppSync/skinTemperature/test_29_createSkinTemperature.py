import requests
from requests_aws4auth import AWS4Auth
import json
import datetime

def test_29_createSkinTemperature_pass(login):
    query = """
    mutation createSkinTemperature($CreateSkinTemperatureInput:CreateSkinTemperatureInput!){
        createSkinTemperature(input:$CreateSkinTemperatureInput){
            user_id
            temperature_timestamp
            temperature_value
        }   
    }


    """
    ct = datetime.datetime.utcnow()
    timestamp = ct.timestamp()
    
    variable = """
    {
    "CreateSkinTemperatureInput": {
        "temperature_timestamp":""" + str(timestamp) +""",
        "temperature_value": 142
        }
    }
    """

    headers = {'x-api-key': login['X_API_KEY'],
            'Authorization': login['Authorization'],
            'Content-Type': "application/graphql"}

    response = requests.post(login['APPSYNC_API_ENDPOINT_URL'], json={
    'query': query, 'variables': variable}, headers=headers)
    
    data = response.json()['data']
    assert data['createSkinTemperature']['temperature_timestamp'] == timestamp
    assert data['createSkinTemperature']['temperature_value'] == 142
    
    print(response.json())
    print(response)
    
    # clean up
    query = """
    mutation deleteSkinTemperature($DeleteSkinTemperatureInput:DeleteSkinTemperatureInput!){
    deleteSkinTemperature(input:$DeleteSkinTemperatureInput){
            user_id
            temperature_timestamp
            temperature_value
        }
    }
    """
    variable = """
    {
        "DeleteSkinTemperatureInput": {
            "temperature_timestamp": """+ str(timestamp) +"""
        }
    }
    """
    headers = {'x-api-key': login['X_API_KEY'],
            'Authorization': login['Authorization'],
            'Content-Type': "application/graphql"}

    response = requests.post(login['APPSYNC_API_ENDPOINT_URL'], json={
        'query': query, 'variables': variable}, headers=headers)