import requests
from requests_aws4auth import AWS4Auth
import json
import datetime

def cr_st(login,count):
    data_list = []
    for i in range(count):
        query = """
         mutation createSkinTemperature($CreateSkinTemperatureInput:CreateSkinTemperatureInput!){
            createSkinTemperature(input:$CreateSkinTemperatureInput){
                user_id
                temperature_timestamp
                temperature_value
            }
        }

        """
        ct = datetime.datetime.utcnow()
        timestamp = ct.timestamp()
        
        variable = """
        {
        "CreateSkinTemperatureInput": {
            "temperature_timestamp":""" + str(timestamp) +""",
            "temperature_value": 143 
        }
        }
        """

        headers = {'x-api-key': login['X_API_KEY'],
                'Authorization': login['Authorization'],
                'Content-Type': "application/graphql"}

        response = requests.post(login['APPSYNC_API_ENDPOINT_URL'], json={
        'query': query, 'variables': variable}, headers=headers)
        
        data = response.json()['data']
        data_list.append(data['createSkinTemperature'])

    return data_list

    
def test_33_querySkinTemperatureByTimestampRange_pass(login):
    data_list = cr_st(login, 3)
    print(data_list)
    query = """
    query querySkinTemperatureByTimestampRange ($from_ts: Float, $to_ts: Float) {
    querySkinTemperatureByTimestampRange(from_ts:$from_ts, to_ts:$to_ts) {
    items {
                user_id
                temperature_timestamp
                temperature_value
            }
        }
    }
    """
    ct = datetime.datetime.utcnow()
    timestamp = ct.timestamp()

    variable = """
    {
        "from_ts": """ + str(data_list[0]['temperature_timestamp']) + """,
        "to_ts": """ + str(data_list[2]['temperature_timestamp']) + """
    }

    """
    headers = {'x-api-key': login['X_API_KEY'],
            'Authorization': login['Authorization'],
            'Content-Type': "application/graphql"}

    response = requests.post(login['APPSYNC_API_ENDPOINT_URL'], json={
    'query': query, 'variables': variable}, headers=headers)
    

    items_ = response.json()['data']['querySkinTemperatureByTimestampRange']['items']
    items = sorted(items_, key=lambda d: d['temperature_timestamp']) 

    print ('length of items is:',len(items))
    assert len(items)  == 3
    for i, entry in enumerate(items):
        assert entry['temperature_timestamp'] == data_list[i]['temperature_timestamp']
        assert entry['temperature_value'] == data_list[i]['temperature_value']
    
    print(response.json())
    print(response)
    # assert False
    
    # end of query blood pressure by time stamp
    
    # clean up
    
    query = """
    mutation deleteSkinTemperature($DeleteSkinTemperatureInput:DeleteSkinTemperatureInput!){
        deleteSkinTemperature(input:$DeleteSkinTemperatureInput){
                user_id
                temperature_timestamp
                temperature_value
            }
        }
        """  
      
   
    # print (data_list)
    # assert False
    for x in data_list:
        variable = """
            {
                "DeleteSkinTemperatureInput": {
                    "temperature_timestamp": """+ str(x['temperature_timestamp']) +"""
                }
            }
            """
        headers = {'x-api-key': login['X_API_KEY'],
                    'Authorization': login['Authorization'],
                    'Content-Type': "application/graphql"}

        response = requests.post(login['APPSYNC_API_ENDPOINT_URL'], json={
                'query': query, 'variables': variable}, headers=headers)
            
        print('attempte to delete: ', x['temperature_timestamp'])
        assert response.status_code == 200
        