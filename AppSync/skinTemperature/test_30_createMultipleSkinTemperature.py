import requests
from requests_aws4auth import AWS4Auth
import json
import datetime

st_list = [
    {'temperature_timestamp': 0.0, 'temperature_value': 97},
    {'temperature_timestamp': 0.0, 'temperature_value': 96}
]


def test_30_createMultipleSkinTemperature_pass(login):
    # put for loop
    for st in st_list:

        query = """
        
        mutation createSkinTemperature($CreateSkinTemperatureInput:CreateSkinTemperatureInput!){
             createSkinTemperature(input:$CreateSkinTemperatureInput){
                user_id
                temperature_timestamp
                temperature_value
            }
        }

        """
        ct = datetime.datetime.utcnow()
        timestamp = ct.timestamp()
        st['temperature_timestamp'] = timestamp

        variable = """
        {
        "CreateSkinTemperatureInput": {
            "temperature_timestamp":""" + str(timestamp) + """,
            "temperature_value": """ + str(st['temperature_value'])+"""
        }
        }
        """

        headers = {'x-api-key': login['X_API_KEY'],
                   'Authorization': login['Authorization'],
                   'Content-Type': "application/graphql"}

        response = requests.post(login['APPSYNC_API_ENDPOINT_URL'], json={
            'query': query, 'variables': variable}, headers=headers)

        print(response.json())
        print(response)
        data = response.json()['data']
        assert data['createSkinTemperature']['temperature_timestamp'] == st['temperature_timestamp']
        assert data['createSkinTemperature']['temperature_value'] == st['temperature_value']

    # clean up
    for st in st_list:
        query = """
        mutation deleteSkinTemperature($DeleteSkinTemperatureInput:DeleteSkinTemperatureInput!){
        deleteSkinTemperature(input:$DeleteSkinTemperatureInput){
                user_id
                temperature_timestamp
                temperature_value
            }
        }
        """
        variable = """
        {
            "DeleteBloodPressureInput": {
                "data_timestamp": """ + str(st['temperature_timestamp']) + """
            }
        }
        """
        headers = {'x-api-key': login['X_API_KEY'],
                   'Authorization': login['Authorization'],
                   'Content-Type': "application/graphql"}

        response = requests.post(login['APPSYNC_API_ENDPOINT_URL'], json={
            'query': query, 'variables': variable}, headers=headers)
