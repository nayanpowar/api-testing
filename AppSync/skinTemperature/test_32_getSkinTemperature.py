import requests
from requests_aws4auth import AWS4Auth

def test_32_getSkinTemperature_pass(login, skin_temperature):
    query = """
   query getSkinTemperature($temperature_timestamp: Float!) {
        getSkinTemperature(temperature_timestamp: $temperature_timestamp) {
            user_id
            temperature_timestamp
            temperature_value
        }
    }
    
    """
    timestamp = skin_temperature['temperature_timestamp']
    variable = """
    {
        "temperature_timestamp":"""+ str(timestamp)+"""

    }
    """

    headers = {'x-api-key': login['X_API_KEY'],
            'Authorization': login['Authorization'],
            'Content-Type': "application/graphql"}

    response = requests.post(login['APPSYNC_API_ENDPOINT_URL'], json={
    'query': query, 'variables': variable}, headers=headers)
    
    data = response.json()['data']
    assert data['getSkinTemperature']['temperature_timestamp'] == skin_temperature['temperature_timestamp'] 
    assert data['getSkinTemperature']['temperature_value'] == skin_temperature['temperature_value']
    print(response.json())
    print(response)