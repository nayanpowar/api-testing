import requests
from requests_aws4auth import AWS4Auth
import json

def test_31_updateSkinTemperature_pass(login, skin_temperature):
    query = """
     mutation updateSkinTemperature($UpdateSkinTemperatureInput:UpdateSkinTemperatureInput!){
        updateSkinTemperature(input:$UpdateSkinTemperatureInput){
            user_id
            temperature_timestamp
            temperature_value
        }
    }
    
    """
    timestamp = skin_temperature['temperature_timestamp']
    variable = """
    {
      "UpdateSkinTemperatureInput": {
        "temperature_timestamp":"""+ str(timestamp)+""",
        "temperature_value": 145
      }
    }
    """

    headers = {'x-api-key': login['X_API_KEY'],
            'Authorization': login['Authorization'],
            'Content-Type': "application/graphql"}

    response = requests.post(login['APPSYNC_API_ENDPOINT_URL'], json={
    'query': query, 'variables': variable}, headers=headers)
    print(response.json())
    print(response)

    data = response.json()['data']
    assert data['updateSkinTemperature']['temperature_timestamp'] == skin_temperature['temperature_timestamp'] 
    assert data['updateSkinTemperature']['temperature_value'] == 145


    query = """
    query getSkinTemperature($temperature_timestamp: Float!) {
        getSkinTemperature(temperature_timestamp: $temperature_timestamp) {
            user_id
            temperature_timestamp
            temperature_value
        }
    }
    
    """
    variable = """
    {
        "temperature_timestamp":"""+ str(timestamp)+"""

    }
    """

    response = requests.post(login['APPSYNC_API_ENDPOINT_URL'], json={
    'query': query, 'variables': variable}, headers=headers)
    
    data = response.json()['data']
    assert data['getSkinTemperature']['temperature_timestamp'] == skin_temperature['temperature_timestamp'] 
    assert data['getSkinTemperature']['temperature_value'] == 145
    print(response.json())
    print(response)
    
    