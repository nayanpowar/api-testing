import pytest
import requests
import json
import datetime


url = "https://blrugreno1.execute-api.us-west-2.amazonaws.com/QAStage/api/users/login"
authorization = 'a64bccc5-1648-46ae-ad78-b0f890f1d6c1'

@pytest.fixture(scope="module")
def login():
    data = {
        'username': 'movano.test@gmail.com',
        'password': 'Csswakad@8782',
        'device_id': '1234'
    }

    headers = {
    'Content-Type': 'application/json',
    'Authorization': authorization
    }

    response = requests.request(
        "POST", url, data=json.dumps(data), headers=headers)
    jsonResponse = response.json()
    return {
        'Authorization': jsonResponse['data']['id_token'],
        'AccessToken': jsonResponse['data']['access_token'],
        'APPSYNC_API_ENDPOINT_URL': "https://twabr4yptbgxjodxlbu43bfj6q.appsync-api.us-west-2.amazonaws.com/graphql",        
        'X_API_KEY': "s2eunynhr5adfi23wtrgqojjmq"
    }
    
@pytest.fixture(scope="module")
def blood_pressure(login):
    query = """
    mutation createBloodPressure($CreateBloodPressureInput:CreateBloodPressureInput!){
        createBloodPressure(input:$CreateBloodPressureInput){
            id
            data_timestamp
            diastolic
            systolic
            pulse_rate
        }
    }

    """
    ct = datetime.datetime.utcnow()
    timestamp = ct.timestamp()
    
    variable = """
    {
        "CreateBloodPressureInput": {
            "data_timestamp":""" + str(timestamp) +""",
            "diastolic": 123,
            "systolic": 145,
            "pulse_rate" : 80
        }
    }
    """

    headers = {'x-api-key': login['X_API_KEY'],
            'Authorization': login['Authorization'],
            'Content-Type': "application/graphql"}

    response = requests.post(login['APPSYNC_API_ENDPOINT_URL'], json={
    'query': query, 'variables': variable}, headers=headers)
    
    data = response.json()['data']
    assert data['createBloodPressure']['data_timestamp'] == timestamp
    assert data['createBloodPressure']['diastolic'] == 123
    assert data['createBloodPressure']['systolic'] == 145
    assert data['createBloodPressure']['pulse_rate'] == 80
    
    

    yield data['createBloodPressure']
    
    query = """
    mutation deleteBloodPressure($DeleteBloodPressureInput:DeleteBloodPressureInput!){
    deleteBloodPressure(input:$DeleteBloodPressureInput){
        id
        data_timestamp
        diastolic
        systolic
        pulse_rate
    }
    }
    """
    variable = """
    {
    "DeleteBloodPressureInput": {
        "data_timestamp": """+ str(timestamp) +"""
    }
    }
    """
    headers = {'x-api-key': login['X_API_KEY'],
            'Authorization': login['Authorization'],
            'Content-Type': "application/graphql"}

    response = requests.post(login['APPSYNC_API_ENDPOINT_URL'], json={
        'query': query, 'variables': variable}, headers=headers)
        
@pytest.fixture(scope="module")
def activity (login):
    query = """
         mutation createActivity($createActivityInput: CreateActivityInput!) {
            createActivity(input: $createActivityInput) {
                act_timestamp
                act_title
                act_value
                act_type
                act_value_unit
                act_note
            }
        }
"""
    ct = datetime.datetime.utcnow()
    timestamp = ct.timestamp()
    
    variable = """
    {
        "createActivityInput": {
            "act_timestamp": """+str(timestamp)+""",
            "act_title": "Cricket",
            "act_value": "1",
            "act_type": "Sport",
            "act_value_unit": "Hrs",
            "act_note": "I played cricket with Sachin"
        }
    }
    """

    headers = {'x-api-key': login['X_API_KEY'],
            'Authorization': login['Authorization'],
            'Content-Type': "application/graphql"}

    response = requests.post(login['APPSYNC_API_ENDPOINT_URL'], json={
    'query': query, 'variables': variable}, headers=headers)
    
    data = response.json()['data']
    assert data['createActivity']['act_timestamp'] == timestamp
    assert data['createActivity']['act_title'] == "Cricket"
    assert data['createActivity']['act_value'] == '1'
    assert data['createActivity']['act_type'] == "Sport"
    assert data['createActivity']['act_value_unit'] == "Hrs"
    assert data['createActivity']['act_note'] == "I played cricket with Sachin"
    
    
    yield data['createActivity']
    
    query = """
        mutation deleteActivity($deleteActivity: DeleteActivityInput!){
            deleteActivity(input: $deleteActivity) {
            act_timestamp
            act_type
            act_value
        }
    }

    """
    variable = """
    {
        "deleteActivity": {
            "act_timestamp": """+str(timestamp)+""",
            "act_type": "Sport",
            "act_title": "Cricket"
        }

    }
    """
    headers = {'x-api-key': login['X_API_KEY'],
            'Authorization': login['Authorization'],
            'Content-Type': "application/graphql"}

    response = requests.post(login['APPSYNC_API_ENDPOINT_URL'], json={
        'query': query, 'variables': variable}, headers=headers)

@pytest.fixture(scope="module")
def pulse_rate(login):
    query = """
    mutation createPulseRate($CreatePulseRateInput:CreatePulseRateInput!){
        createPulseRate(input:$CreatePulseRateInput){
            user_id
            pulse_rate_timestamp
            pulse_rate_value
        }
    }


    """
    ct = datetime.datetime.utcnow()
    timestamp = ct.timestamp()
    
    variable = """
    {
    "CreatePulseRateInput": {
            "pulse_rate_timestamp": """+str(timestamp)+""",
            "pulse_rate_value": 142
        }
    }
    """

    headers = {'x-api-key': login['X_API_KEY'],
            'Authorization': login['Authorization'],
            'Content-Type': "application/graphql"}

    response = requests.post(login['APPSYNC_API_ENDPOINT_URL'], json={
    'query': query, 'variables': variable}, headers=headers)
    
    data = response.json()['data']
    assert data['createPulseRate']['pulse_rate_timestamp'] == timestamp
    assert data['createPulseRate']['pulse_rate_value'] == 142
   
    print(response.json())
    print(response)
    
    

    yield data['createPulseRate']
    
    query = """
    mutation deletePulseRate($DeletePulseRateInput:DeletePulseRateInput!){
        deletePulseRate(input:$DeletePulseRateInput){
            user_id
            pulse_rate_timestamp
            pulse_rate_value
        }   
    }

    """
    variable = """
    {
        "DeletePulseRateInput": {
            "pulse_rate_timestamp":"""+str(timestamp)+"""
        }   
    }

    """
    headers = {'x-api-key': login['X_API_KEY'],
            'Authorization': login['Authorization'],
            'Content-Type': "application/graphql"}

    response = requests.post(login['APPSYNC_API_ENDPOINT_URL'], json={
        'query': query, 'variables': variable}, headers=headers)

@pytest.fixture(scope="module")
def step_count(login):
    query = """
    mutation createStepCount($CreateStepCountInput:CreateStepCountInput!){
        createStepCount(input:$CreateStepCountInput){
            user_id
            step_count_timestamp
            step_count_value
        }
    }
    """
    ct = datetime.datetime.utcnow()
    timestamp = ct.timestamp()
    
    variable = """
    {
        "CreateStepCountInput": {
        "step_count_timestamp":"""+str(timestamp)+""",
        "step_count_value": 554
        }
    }

    """

    headers = {'x-api-key': login['X_API_KEY'],
            'Authorization': login['Authorization'],
            'Content-Type': "application/graphql"}

    response = requests.post(login['APPSYNC_API_ENDPOINT_URL'], json={
    'query': query, 'variables': variable}, headers=headers)
    
    data = response.json()['data']
    assert data['createStepCount']['step_count_timestamp'] == timestamp
    assert data['createStepCount']['step_count_value'] == 554
   
    print(response.json())
    print(response)
    
    yield data['createStepCount']

    query = """
    mutation deleteStepCount($DeleteStepCountInput:DeleteStepCountInput!){
        deleteStepCount(input:$DeleteStepCountInput){
            user_id
            step_count_timestamp
            step_count_value
        }
    }
    """
    variable = """
    {
        "DeleteStepCountInput": {
            "step_count_timestamp": """+str(timestamp)+"""
        }
    }


    """
    headers = {'x-api-key': login['X_API_KEY'],
            'Authorization': login['Authorization'],
            'Content-Type': "application/graphql"}

    response = requests.post(login['APPSYNC_API_ENDPOINT_URL'], json={
        'query': query, 'variables': variable}, headers=headers)

@pytest.fixture(scope="module")
def breathing_rate(login):
    query = """
    mutation createBreathingRate($CreateBreathingRateInput:CreateBreathingRateInput!){
    createBreathingRate(input:$CreateBreathingRateInput){
          user_id
          breathing_rate_timestamp
          breathing_rate_value
        }
    }
"""
    ct = datetime.datetime.now()
    timestamp = ct.timestamp()
    
    variable = """
    {
        "CreateBreathingRateInput": {
            "breathing_rate_timestamp": """+ str(timestamp) +""",
            "breathing_rate_value": 142
        }
    }
    """

    headers = {'x-api-key': login['X_API_KEY'],
            'Authorization': login['Authorization'],
            'Content-Type': "application/graphql"}

    response = requests.post(login['APPSYNC_API_ENDPOINT_URL'], json={
    'query': query, 'variables': variable}, headers=headers)
    
    data = response.json()['data']
    assert data['createBreathingRate']['breathing_rate_timestamp'] == timestamp
    assert data['createBreathingRate']['breathing_rate_value'] == 142
   
    print(response.json())
    print(response)
    assert True
    
    yield data['createBreathingRate']

    query = """
     mutation deleteBreathingRate($DeleteBreathingRateInput:DeleteBreathingRateInput!){
       deleteBreathingRate(input:$DeleteBreathingRateInput){
          user_id
          breathing_rate_timestamp
          breathing_rate_value
        }
    }

    """
    ct = datetime.datetime.now()
    timestamp = ct.timestamp()
    
    variable = """
            {
                "DeleteBreathingRateInput": {
                    "breathing_rate_timestamp": """+ str(timestamp) +"""
                }
            }
      """
    headers = {'x-api-key': login['X_API_KEY'],
            'Authorization': login['Authorization'],
            'Content-Type': "application/graphql"}

    response = requests.post(login['APPSYNC_API_ENDPOINT_URL'], json={
        'query': query, 'variables': variable}, headers=headers)

@pytest.fixture(scope="module")
def pause_data(login):
    query = """
    mutation createPauseData($CreatePauseDataInput:CreatePauseDataInput!){
    createPauseData(input:$CreatePauseDataInput){
            id
            user_id
            start_time
            end_time
            average_pulse_rate
            average_skin_temprature
            average_systolic
            average_diastolic
        }
    }
    """
    dt = datetime.datetime.now(datetime.timezone.utc)
  
    utc_time = dt.replace(tzinfo=datetime.timezone.utc)
    end_time =  utc_time.timestamp()
    start_time = end_time - 50*1*60
    
    
    variable = """
    {
        "CreatePauseDataInput": {
            "start_time": 1637724187.189696,
            "end_time": 1637724187.189696,
            "average_pulse_rate":1700,
            "average_skin_temprature":97,
            "average_systolic":110,
            "average_diastolic":85
        }
    }
    """

    headers = {'x-api-key': login['X_API_KEY'],
            'Authorization': login['Authorization'],
            'Content-Type': "application/graphql"}

    response = requests.post(login['APPSYNC_API_ENDPOINT_URL'], json={
    'query': query, 'variables': variable}, headers=headers)
    
    assert response.status_code == 200
    data = response.json()['data']
    print (data)
    assert data['createPauseData']['start_time'] == 1637724187.189696
    assert data['createPauseData']['end_time'] == 1637724187.189696
    assert data['createPauseData']['average_pulse_rate'] == 1700
    assert data['createPauseData']['average_skin_temprature'] == 97
    assert data['createPauseData']['average_systolic'] == 110
    assert data['createPauseData']['average_diastolic'] == 85
    
    print(response.json())
    print(response)

    yield data['createPauseData']

    query = """
    mutation deletePauseData($DeletePauseDataInput:DeletePauseDataInput!){
    deletePauseData(input:$DeletePauseDataInput){
      id
      user_id
      start_time
      end_time
      average_pulse_rate
      average_skin_temprature
      average_systolic
      average_diastolic
      }
    }

    """
    variable = """
    {
        "DeletePauseDataInput": {
            "id": "4fedab87-ed79-4b80-b12e-859bb424b23c"

        }
    }
    """
    headers = {'x-api-key': login['X_API_KEY'],
            'Authorization': login['Authorization'],
            'Content-Type': "application/graphql"}

    response = requests.post(login['APPSYNC_API_ENDPOINT_URL'], json={
        'query': query, 'variables': variable}, headers=headers) 

@pytest.fixture(scope="module")
def skin_temperature(login):
    query = """
    mutation createSkinTemperature($CreateSkinTemperatureInput:CreateSkinTemperatureInput!){
        createSkinTemperature(input:$CreateSkinTemperatureInput){
            user_id
            temperature_timestamp
            temperature_value
        }   
    }


    """
    ct = datetime.datetime.utcnow()
    timestamp = ct.timestamp()
    
    variable = """
    {
    "CreateSkinTemperatureInput": {
        "temperature_timestamp":""" + str(timestamp) +""",
        "temperature_value": 142
        }
    }
    """

    headers = {'x-api-key': login['X_API_KEY'],
            'Authorization': login['Authorization'],
            'Content-Type': "application/graphql"}

    response = requests.post(login['APPSYNC_API_ENDPOINT_URL'], json={
    'query': query, 'variables': variable}, headers=headers)
    
    data = response.json()['data']
    assert data['createSkinTemperature']['temperature_timestamp'] == timestamp
    assert data['createSkinTemperature']['temperature_value'] == 142
    
    print(response.json())
    print(response)

    yield data['createSkinTemperature']

    query = """
    mutation deleteSkinTemperature($DeleteSkinTemperatureInput:DeleteSkinTemperatureInput!){
    deleteSkinTemperature(input:$DeleteSkinTemperatureInput){
            user_id
            temperature_timestamp
            temperature_value
        }
    }
    """
    variable = """
    {
        "DeleteSkinTemperatureInput": {
            "temperature_timestamp": """+ str(timestamp) +"""
        }
    }
    """
    headers = {'x-api-key': login['X_API_KEY'],
            'Authorization': login['Authorization'],
            'Content-Type': "application/graphql"}

    response = requests.post(login['APPSYNC_API_ENDPOINT_URL'], json={
        'query': query, 'variables': variable}, headers=headers)