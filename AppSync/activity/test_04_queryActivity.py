import requests
from requests_aws4auth import AWS4Auth
import json
import datetime

def cr_ac(login,count):
    data_list = []
    for i in range(count):
        query = """
        mutation createActivity($createActivityInput: CreateActivityInput!) {
            createActivity(input: $createActivityInput) {
                act_timestamp
                act_title
                act_value
                act_type
                act_value_unit
                act_note
            }
        }

        """
        ct = datetime.datetime.utcnow()
        timestamp = ct.timestamp()
        
        variable = """
        {
        "createActivityInput": {
            "act_timestamp": """+str(timestamp)+""",
            "act_title": "Cricket",
            "act_value": "1",
            "act_type": "Sport",
            "act_value_unit": "Hrs",
            "act_note": "I played cricket with Sachin"
        }
        }
        """

        headers = {'x-api-key': login['X_API_KEY'],
                'Authorization': login['Authorization'],
                'Content-Type': "application/graphql"}

        response = requests.post(login['APPSYNC_API_ENDPOINT_URL'], json={
        'query': query, 'variables': variable}, headers=headers)
        
        data = response.json()['data']
        data_list.append(data['createActivity'])

    return data_list

    
def test_04_queryActivity_pass(login):
    data_list = cr_ac(login, 3)
    print(data_list)
    query = """
    query queryActivitiesByTimestampRange ($from_ts: Float, $to_ts: Float) {
    queryActivitiesByTimestampRange(from_ts:$from_ts, to_ts:$to_ts) {
    items {
                act_title
                act_value
                act_value_unit
                act_timestamp
                act_note
                act_type
            }
        }
    }
    """
    ct = datetime.datetime.utcnow()
    timestamp = ct.timestamp()

    variable = """
    {
        "from_ts": """ + str(data_list[0]['act_timestamp']) + """,
        "to_ts": """ + str(data_list[2]['act_timestamp']) + """
    }

    """
    headers = {'x-api-key': login['X_API_KEY'],
            'Authorization': login['Authorization'],
            'Content-Type': "application/graphql"}

    response = requests.post(login['APPSYNC_API_ENDPOINT_URL'], json={
    'query': query, 'variables': variable}, headers=headers)
    

    items_ = response.json()['data']['queryActivitiesByTimestampRange']['items']
    items = sorted(items_, key=lambda d: d['act_timestamp']) 

    print ('length of items is:',len(items))
    assert len(items)  == 3
    for i, entry in enumerate(items):
        assert entry['act_timestamp'] == data_list[i]['act_timestamp']
        assert entry['act_title'] == data_list[i]['act_title']
        assert entry['act_value'] == data_list[i]['act_value']
        assert entry['act_value_unit'] == data_list[i]['act_value_unit']
        assert entry['act_note'] == data_list[i]['act_note']
        assert entry['act_type'] == data_list[i]['act_type']


    print(response.json())
    print(response)
    # assert False
    
    # end of query blood pressure by time stamp
    
    # clean up
    
    query = """
    mutation deleteActivity($deleteActivity: DeleteActivityInput!){
        deleteActivity(input: $deleteActivity) {
                act_timestamp
                act_type
                act_value
            }
        }
        """  
      
   
    # print (data_list)
    # assert False
    for x in data_list:
        variable = """
            {
                "DeleteActivityInput": {
                    "act_timestamp": """+ str(x['act_timestamp']) +"""
                }
            }
            """
        headers = {'x-api-key': login['X_API_KEY'],
                    'Authorization': login['Authorization'],
                    'Content-Type': "application/graphql"}

        response = requests.post(login['APPSYNC_API_ENDPOINT_URL'], json={
                'query': query, 'variables': variable}, headers=headers)
            
        print('attempte to delete: ', x['act_timestamp'])
        assert response.status_code == 200
        