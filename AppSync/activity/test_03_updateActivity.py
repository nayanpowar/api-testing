import requests
from requests_aws4auth import AWS4Auth
import json
import datetime
from conftest import activity
from conftest import login

def test_createActivity_pass(login):
    query = """
      mutation createActivity($createActivityInput: CreateActivityInput!) {
       createActivity(input: $createActivityInput) {
         act_timestamp
         act_title
         act_value
         act_type
         act_value_unit
         act_note
       }
     }
    
    """
    ct = datetime.datetime.now()
    timestamp = ct.timestamp()
    variable = """
    {
      "createActivityInput": {
                 "act_timestamp": """+str(timestamp)+""",
                 "act_title": "Cricket",
                 "act_value": "1",
                 "act_type": "Sport",
                 "act_value_unit": "Hrs",
                 "act_note": "I played cricket with Sachin"
             }
    }
    """

    headers = {'x-api-key': login['X_API_KEY'],
            'Authorization': login['Authorization'],
            'Content-Type': "application/graphql"}

    response = requests.post(login['APPSYNC_API_ENDPOINT_URL'], json={
    'query': query, 'variables': variable}, headers=headers)
    print(response.json())
    print(response)

    data = response.json()['data']
    assert data['createActivity']['act_timestamp'] == timestamp
    assert data['createActivity']['act_title'] == "Cricket"
    assert data['createActivity']['act_value'] == '1'
    assert data['createActivity']['act_type'] == "Sport"
    assert data['createActivity']['act_value_unit'] == "Hrs"
    assert data['createActivity']['act_note'] == "I played cricket with Sachin"

    print(response.json())
    print(response)
    assert True


def test_03_updateActivity(login, activity):
    query = """
     mutation updateActivity($updateActivity: UpdateActivityInput!) {
             updateActivity(input: $updateActivity) {
                act_timestamp
                act_type
                act_title
                act_note
            }
        }
    
    """
    timestamp = activity['act_timestamp']
    variable = """
    {
     "updateActivity": {
                "act_timestamp": """ + str(timestamp)+""",
                "act_type": "Sport",
                "act_title": "Cricket",
                "act_note": "I played cricket with Sachin and with Dhoni also"
            }
    }
    """

    headers = {'x-api-key': login['X_API_KEY'],
            'Authorization': login['Authorization'],
            'Content-Type': "application/graphql"}

    response = requests.post(login['APPSYNC_API_ENDPOINT_URL'], json={
    'query': query, 'variables': variable}, headers=headers)
    print(response.json())
    print(response)

    data = response.json()['data']
    assert data['updateActivity']['act_timestamp'] == activity['act_timestamp']
    assert data['updateActivity']['act_type'] == "Sport"
    assert data['updateActivity']['act_title'] == "Cricket"
    assert data['updateActivity']['act_note'] == "I played cricket with Sachin and with Dhoni also"
    
    query = """
     mutation deleteActivity($deleteActivity: DeleteActivityInput!){
         deleteActivity(input: $deleteActivity) {
         act_timestamp
         act_type
         act_value
        }
    }
    
    """
    timestamp = activity['act_timestamp']
    variable = """
    {
     "deleteActivity": {
               "act_timestamp": """+str(timestamp)+""",
               "act_type": "Sport",
               "act_title": "Cricket"
             }
    }
    """

    headers = {'x-api-key': login['X_API_KEY'],
            'Authorization': login['Authorization'],
            'Content-Type': "application/graphql"}

    response = requests.post(login['APPSYNC_API_ENDPOINT_URL'], json={
    'query': query, 'variables': variable}, headers=headers)