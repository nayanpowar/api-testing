import requests
from requests_aws4auth import AWS4Auth
import json
import datetime

def test_05_createActivity_pass(login):
    query = """
    query listActivities {
    listActivities {
        items {
        act_timestamp
        act_title
        act_value
        act_value_unit
        act_note
    }
  }
}

"""
    

    headers = {'x-api-key': login['X_API_KEY'],
            'Authorization': login['Authorization'],
            'Content-Type': "application/graphql"}

    response = requests.post(login['APPSYNC_API_ENDPOINT_URL'], json={
    'query': query}, headers=headers)
    
    data = response.json()['data']
    # assert data['createActivity']['act_timestamp'] == timestamp
    # assert data['createActivity']['act_title'] == "Cricket"
    # assert data['createActivity']['act_value'] == '1'
    # assert data['createActivity']['act_type'] == "Sport"
    # assert data['createActivity']['act_value_unit'] == "Hrs"
    # assert data['createActivity']['act_note'] == "I played cricket with Sachin"
    
    
    print(response.json())
    print(response)
    # assert False