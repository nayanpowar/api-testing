import requests
from requests_aws4auth import AWS4Auth
import json

def test_25_updateStepCount(login, step_count):
    query = """
     mutation updateStepCount($UpdateStepCountInput:UpdateStepCountInput!){
        updateStepCount(input:$UpdateStepCountInput){
            user_id
            step_count_timestamp
            step_count_value
        }
    }
    
    """
    timestamp = step_count['step_count_timestamp']
    variable = """
     {
      "UpdateStepCountInput": {
        "step_count_timestamp":"""+ str(timestamp)+""",
        "step_count_value": 556
      }
    }
    """

    headers = {'x-api-key': login['X_API_KEY'],
            'Authorization': login['Authorization'],
            'Content-Type': "application/graphql"}

    response = requests.post(login['APPSYNC_API_ENDPOINT_URL'], json={
    'query': query, 'variables': variable}, headers=headers)
    print(response.json())
    print(response)

    data = response.json()['data']
    assert data['updateStepCount']['step_count_timestamp'] == step_count['step_count_timestamp'] 
    assert data['updateStepCount']['step_count_value'] == 556
    
    query = """
    query getStepCount($step_count_timestamp: Float!) {
        getStepCount(step_count_timestamp: $step_count_timestamp) {
            user_id
            step_count_timestamp
            step_count_value
        }
    }
    
    """
    variable = """
    {
        "step_count_timestamp":"""+str(timestamp)+"""

    }
    """

    response = requests.post(login['APPSYNC_API_ENDPOINT_URL'], json={
    'query': query, 'variables': variable}, headers=headers)
    
    data = response.json()['data']
    assert data['getStepCount']['step_count_timestamp'] == step_count['step_count_timestamp'] 
    assert data['getStepCount']['step_count_value'] == 556
   
    print(response.json())
    print(response)
    
    