import requests
from requests_aws4auth import AWS4Auth
import json
import datetime

def test_28_listStepCounts_pass(login):
    query = """
    query listStepCounts{
    listStepCounts{
        items{
        user_id
        step_count_timestamp
        step_count_value
    }
  }
}

"""
    

    headers = {'x-api-key': login['X_API_KEY'],
            'Authorization': login['Authorization'],
            'Content-Type': "application/graphql"}

    response = requests.post(login['APPSYNC_API_ENDPOINT_URL'], json={
    'query': query}, headers=headers)
    
    data = response.json()['data']
      
    
    print(response.json())
    print(response)
    