import requests
from requests_aws4auth import AWS4Auth
import json
import datetime

def cr_sc(login,count):
    data_list = []
    for i in range(count):
        query = """
        mutation createStepCount($CreateStepCountInput:CreateStepCountInput!){
            createStepCount(input:$CreateStepCountInput){
                user_id
                step_count_timestamp
                step_count_value
            }
        }

        """
        ct = datetime.datetime.utcnow()
        timestamp = ct.timestamp()
        
        variable = """
        {
        "CreateStepCountInput": {
            "step_count_timestamp":"""+str(timestamp)+""",
            "step_count_value": 554
        }
        }
        """

        headers = {'x-api-key': login['X_API_KEY'],
                'Authorization': login['Authorization'],
                'Content-Type': "application/graphql"}

        response = requests.post(login['APPSYNC_API_ENDPOINT_URL'], json={
        'query': query, 'variables': variable}, headers=headers)
        
        data = response.json()['data']
        data_list.append(data['createStepCount'])

    return data_list

    
def test_27_queryBloodPressureByTimestampRange_pass(login):
    data_list = cr_sc(login, 3)
    print(data_list)
    query = """
    query queryStepCountByTimestampRange ($from_ts: Float, $to_ts: Float) {
    queryStepCountByTimestampRange(from_ts:$from_ts, to_ts:$to_ts) {
    items {
                user_id
                step_count_timestamp
                step_count_value
            }
        }
    }
    """
    ct = datetime.datetime.utcnow()
    timestamp = ct.timestamp()

    variable = """
    {
        "from_ts": """ + str(data_list[0]['step_count_timestamp']) + """,
        "to_ts": """ + str(data_list[2]['step_count_timestamp']) + """
    }

    """
    headers = {'x-api-key': login['X_API_KEY'],
            'Authorization': login['Authorization'],
            'Content-Type': "application/graphql"}

    response = requests.post(login['APPSYNC_API_ENDPOINT_URL'], json={
    'query': query, 'variables': variable}, headers=headers)
    

    items_ = response.json()['data']['queryStepCountByTimestampRange']['items']
    items = sorted(items_, key=lambda d: d['step_count_timestamp']) 

    print ('length of items is:',len(items))
    assert len(items)  == 3
    for i, entry in enumerate(items):
        assert entry['step_count_timestamp'] == data_list[i]['step_count_timestamp']
        assert entry['step_count_value'] == data_list[i]['step_count_value']
        
    print(response.json())
    print(response)
    # assert False
    
    # end of query blood pressure by time stamp
    
    # clean up
    
    query = """
    mutation deleteStepCount($DeleteStepCountInput:DeleteStepCountInput!){
        deleteStepCount(input:$DeleteStepCountInput){
                user_id
                step_count_timestamp
                step_count_value
            }
        }
        """  
      
   
    # print (data_list)
    # assert False
    for x in data_list:
        variable = """
            {
                "DeleteStepCountInput": {
                    "step_count_timestamp": """+ str(x['step_count_timestamp']) +"""
                }
            }
            """
        headers = {'x-api-key': login['X_API_KEY'],
                    'Authorization': login['Authorization'],
                    'Content-Type': "application/graphql"}

        response = requests.post(login['APPSYNC_API_ENDPOINT_URL'], json={
                'query': query, 'variables': variable}, headers=headers)
            
        print('attempte to delete: ', x['step_count_timestamp'])
        assert response.status_code == 200
        