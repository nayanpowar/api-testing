import requests
from requests_aws4auth import AWS4Auth
import json
import datetime

def test_23_createStepCount_pass(login):
    query = """
    mutation createStepCount($CreateStepCountInput:CreateStepCountInput!){
        createStepCount(input:$CreateStepCountInput){
            user_id
            step_count_timestamp
            step_count_value
        }
    }
    """
    ct = datetime.datetime.utcnow()
    timestamp = ct.timestamp()
    
    variable = """
    {
        "CreateStepCountInput": {
        "step_count_timestamp":"""+str(timestamp)+""",
        "step_count_value": 554
        }
    }

    """

    headers = {'x-api-key': login['X_API_KEY'],
            'Authorization': login['Authorization'],
            'Content-Type': "application/graphql"}

    response = requests.post(login['APPSYNC_API_ENDPOINT_URL'], json={
    'query': query, 'variables': variable}, headers=headers)
    
    data = response.json()['data']
    assert data['createStepCount']['step_count_timestamp'] == timestamp
    assert data['createStepCount']['step_count_value'] == 554
   
    print(response.json())
    print(response)
    
    # clean up
    query = """
    mutation deleteStepCount($DeleteStepCountInput:DeleteStepCountInput!){
        deleteStepCount(input:$DeleteStepCountInput){
            user_id
            step_count_timestamp
            step_count_value
        }
    }
    """
    variable = """
    {
        "DeleteStepCountInput": {
            "step_count_timestamp": """+str(timestamp)+"""
        }
    }


    """
    headers = {'x-api-key': login['X_API_KEY'],
            'Authorization': login['Authorization'],
            'Content-Type': "application/graphql"}

    response = requests.post(login['APPSYNC_API_ENDPOINT_URL'], json={
        'query': query, 'variables': variable}, headers=headers)