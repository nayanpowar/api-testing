import requests
from requests_aws4auth import AWS4Auth

def test_26_getStepCount(login, step_count):
    query = """
    query getStepCount($step_count_timestamp: Float!) {
        getStepCount(step_count_timestamp: $step_count_timestamp) {
            user_id
            step_count_timestamp
            step_count_value
        }
    }
    
    """
    timestamp = step_count['step_count_timestamp']
    variable = """
    {
        "step_count_timestamp":"""+ str(timestamp)+"""

    }
    """

    headers = {'x-api-key': login['X_API_KEY'],
            'Authorization': login['Authorization'],
            'Content-Type': "application/graphql"}

    response = requests.post(login['APPSYNC_API_ENDPOINT_URL'], json={
    'query': query, 'variables': variable}, headers=headers)
    
    data = response.json()['data']
    assert data['getStepCount']['step_count_timestamp'] == timestamp 
    assert data['getStepCount']['step_count_value'] == 554
   
    print(response.json())
    print(response)