import requests
from requests_aws4auth import AWS4Auth
import json
import datetime
sc_list = [
    {'step_count_timestamp': 0.0, 'step_count_value': 654 },
    {'step_count_timestamp': 0.0, 'step_count_value': 655 },
]


def test_24_createMultipleStepCount_pass(login):
    for sc in sc_list:
        query = """
        mutation createStepCount($CreateStepCountInput:CreateStepCountInput!){
            createStepCount(input:$CreateStepCountInput){
                user_id
                step_count_timestamp
                step_count_value
            }
        }
        """
        ct = datetime.datetime.utcnow()
        timestamp = ct.timestamp()
        sc['step_count_timestamp'] = timestamp

        variable = """
        {
            "CreateStepCountInput": {
            "step_count_timestamp":"""+str(timestamp)+""",
            "step_count_value": """ + str(sc['step_count_value'])+"""
            }
        }

        """

        headers = {'x-api-key': login['X_API_KEY'],
                'Authorization': login['Authorization'],
                'Content-Type': "application/graphql"}

        response = requests.post(login['APPSYNC_API_ENDPOINT_URL'], json={
        'query': query, 'variables': variable}, headers=headers)
        
        data = response.json()['data']
        assert data['createStepCount']['step_count_timestamp'] == sc['step_count_timestamp']
        assert data['createStepCount']['step_count_value'] == sc['step_count_value']
    
        print(response.json())
        print(response)

    for bp in sc_list:
        query = """
        mutation deleteStepCount($DeleteStepCountInput:DeleteStepCountInput!){
            deleteStepCount(input:$DeleteStepCountInput){
                user_id
                step_count_timestamp
                step_count_value
            }
        }
        """
        variable = """
        {
            "DeleteStepCountInput": {
                "step_count_timestamp": """ + str(sc['step_count_timestamp']) + """
            }
        }
        """
        headers = {'x-api-key': login['X_API_KEY'],
                   'Authorization': login['Authorization'],
                   'Content-Type': "application/graphql"}

        response = requests.post(login['APPSYNC_API_ENDPOINT_URL'], json={
            'query': query, 'variables': variable}, headers=headers)
