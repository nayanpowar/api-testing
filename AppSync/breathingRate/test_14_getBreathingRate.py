import requests
from requests_aws4auth import AWS4Auth
import json
import datetime
def test_14_createBreathingRate_pass(login):
    query = """
    mutation createBreathingRate($CreateBreathingRateInput:CreateBreathingRateInput!){
    createBreathingRate(input:$CreateBreathingRateInput){
          user_id
          breathing_rate_timestamp
          breathing_rate_value
        }
    }
"""
    ct = datetime.datetime.now()
    timestamp = ct.timestamp()
    
    variable = """
    {
        "CreateBreathingRateInput": {
            "breathing_rate_timestamp": """+ str(timestamp) +""",
            "breathing_rate_value": 142
        }
    }
    """

    headers = {'x-api-key': login['X_API_KEY'],
            'Authorization': login['Authorization'],
            'Content-Type': "application/graphql"}

    response = requests.post(login['APPSYNC_API_ENDPOINT_URL'], json={
    'query': query, 'variables': variable}, headers=headers)
    
    data = response.json()['data']
    assert data['createBreathingRate']['breathing_rate_timestamp'] == timestamp
    assert data['createBreathingRate']['breathing_rate_value'] == 142
   
    print(response.json())
    print(response)
    assert True


def test_getBreathingRate_pass(login, breathing_rate):
    query = """
    query getBreathingRate($breathing_rate_timestamp: Float!) {
        getBreathingRate(breathing_rate_timestamp: $breathing_rate_timestamp) {
            user_id
            breathing_rate_timestamp
            breathing_rate_value
            
        }
    }
    
"""
    timestamp = breathing_rate['breathing_rate_timestamp']
    
    
    variable = """
     {
        "breathing_rate_timestamp":  """+str(timestamp)+"""
    }
    """

    headers = {'x-api-key': login['X_API_KEY'],
            'Authorization': login['Authorization'],
            'Content-Type': "application/graphql"}

    response = requests.post(login['APPSYNC_API_ENDPOINT_URL'], json={
    'query': query, 'variables': variable}, headers=headers)
    
    data = response.json()['data']
    assert data['getBreathingRate']['breathing_rate_timestamp'] == breathing_rate['breathing_rate_timestamp']
    assert data['getBreathingRate']['breathing_rate_value'] ==breathing_rate['breathing_rate_value']
    
   
    print(response.json())
    print(response)
    # assert False