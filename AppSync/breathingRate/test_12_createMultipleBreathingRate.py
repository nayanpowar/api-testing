import requests
from requests_aws4auth import AWS4Auth
import json
import datetime

br_list = [
    {'breathing_rate_timestamp': 0.0, 'breathing_rate_value':143 },
    {'breathing_rate_timestamp': 0.0, 'breathing_rate_value':144 },
    {'breathing_rate_timestamp': 0.0, 'breathing_rate_value':145 },
]


def test_12_createBreathingRate_pass(login):
    # put for loop
    for br in br_list:

        query = """
        
            mutation createBreathingRate($CreateBreathingRateInput:CreateBreathingRateInput!){
            createBreathingRate(input:$CreateBreathingRateInput){
                user_id
                breathing_rate_timestamp
                breathing_rate_value
            }
        }


        """
        ct = datetime.datetime.utcnow()
        timestamp = ct.timestamp()
        br['breathing_rate_timestamp'] = timestamp

        variable = """
        {
            "CreateBreathingRateInput": {
                "breathing_rate_timestamp": """+str(timestamp)+""",
                "breathing_rate_value": """+str(br['breathing_rate_value'])+"""
            }
        }
        """

        headers = {'x-api-key': login['X_API_KEY'],
                   'Authorization': login['Authorization'],
                   'Content-Type': "application/graphql"}

        response = requests.post(login['APPSYNC_API_ENDPOINT_URL'], json={
            'query': query, 'variables': variable}, headers=headers)

        print(response.json())
        print(response)
        data = response.json()['data']
        assert data['createBreathingRate']['breathing_rate_timestamp'] == br['breathing_rate_timestamp']
        assert data['createBreathingRate']['breathing_rate_value'] == br['breathing_rate_value']
        

    # clean up
    for hr in br_list:
        query = """
        mutation deleteBreathingRate($DeleteBreathingRateInput:DeleteBreathingRateInput!){
            deleteBreathingRate(input:$DeleteBreathingRateInput){
                user_id
                breathing_rate_timestamp
                breathing_rate_value
            }
        }
        """
        variable = """
        {
            "DeleteBreathingRateInput": {
                "breathing_rate_timestamp": """+ str(hr['breathing_rate_timestamp']) +"""
            }
        }
        """
        
        headers = {'x-api-key': login['X_API_KEY'],
                'Authorization': login['Authorization'],
                'Content-Type': "application/graphql"}

        response = requests.post(login['APPSYNC_API_ENDPOINT_URL'], json={
            'query': query, 'variables': variable}, headers=headers)