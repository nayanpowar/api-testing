import requests
from requests_aws4auth import AWS4Auth
import json
import datetime

def test_11_createBreathingRate_pass(login):
    query = """
    mutation createBreathingRate($CreateBreathingRateInput:CreateBreathingRateInput!){
    createBreathingRate(input:$CreateBreathingRateInput){
          user_id
          breathing_rate_timestamp
          breathing_rate_value
        }
    }
"""
    ct = datetime.datetime.now()
    timestamp = ct.timestamp()
    
    variable = """
    {
        "CreateBreathingRateInput": {
            "breathing_rate_timestamp": """+ str(timestamp) +""",
            "breathing_rate_value": 142
        }
    }
    """

    headers = {'x-api-key': login['X_API_KEY'],
            'Authorization': login['Authorization'],
            'Content-Type': "application/graphql"}

    response = requests.post(login['APPSYNC_API_ENDPOINT_URL'], json={
    'query': query, 'variables': variable}, headers=headers)
    
    data = response.json()['data']
    assert data['createBreathingRate']['breathing_rate_timestamp'] == timestamp
    assert data['createBreathingRate']['breathing_rate_value'] == 142
   
    print(response.json())
    print(response)
    assert True
    
    # clean up
    query = """
     mutation deleteBreathingRate($DeleteBreathingRateInput:DeleteBreathingRateInput!){
       deleteBreathingRate(input:$DeleteBreathingRateInput){
          user_id
          breathing_rate_timestamp
          breathing_rate_value
        }
    }

    """
    ct = datetime.datetime.now()
    timestamp = ct.timestamp()
    
    variable = """
            {
                "DeleteBreathingRateInput": {
                    "breathing_rate_timestamp": """+ str(timestamp) +"""
                }
            }
      """
    headers = {'x-api-key': login['X_API_KEY'],
            'Authorization': login['Authorization'],
            'Content-Type': "application/graphql"}

    response = requests.post(login['APPSYNC_API_ENDPOINT_URL'], json={
        'query': query, 'variables': variable}, headers=headers)