import requests
from requests_aws4auth import AWS4Auth
import json
import datetime

def cr_br(login,count):
    data_list = []
    for i in range(count):
        query = """
        mutation createBreathingRate($CreateBreathingRateInput:CreateBreathingRateInput!){
            createBreathingRate(input:$CreateBreathingRateInput){
                user_id
                breathing_rate_timestamp
                breathing_rate_value
            }
        }

        """
        ct = datetime.datetime.utcnow()
        timestamp = ct.timestamp()
        
        variable = """
        {
        "CreateBreathingRateInput": {
            "breathing_rate_timestamp": """+ str(timestamp) +""",
            "breathing_rate_value": 142
        }
        }
        """

        headers = {'x-api-key': login['X_API_KEY'],
                'Authorization': login['Authorization'],
                'Content-Type': "application/graphql"}

        response = requests.post(login['APPSYNC_API_ENDPOINT_URL'], json={
        'query': query, 'variables': variable}, headers=headers)
        
        data = response.json()['data']
        data_list.append(data['createBreathingRate'])

    return data_list

    
def test_15_queryBloodPressureByTimestampRange_pass(login):
    data_list = cr_br(login, 3)
    print(data_list)
    query = """
    query queryBreathingRateByTimestampRange ($from_ts: Float, $to_ts: Float) {
    queryBreathingRateByTimestampRange(from_ts:$from_ts, to_ts:$to_ts) {
    items {
                user_id
                breathing_rate_timestamp
                breathing_rate_value
            }
        }
    }
    """
    ct = datetime.datetime.utcnow()
    timestamp = ct.timestamp()

    variable = """
    {
        "from_ts": """ + str(data_list[0]['breathing_rate_timestamp']) + """,
        "to_ts": """ + str(data_list[2]['breathing_rate_timestamp']) + """
    }

    """
    headers = {'x-api-key': login['X_API_KEY'],
            'Authorization': login['Authorization'],
            'Content-Type': "application/graphql"}

    response = requests.post(login['APPSYNC_API_ENDPOINT_URL'], json={
    'query': query, 'variables': variable}, headers=headers)
    

    items_ = response.json()['data']['queryBreathingRateByTimestampRange']['items']
    items = sorted(items_, key=lambda d: d['breathing_rate_timestamp']) 

    print ('length of items is:',len(items))
    assert len(items)  == 3
    for i, entry in enumerate(items):
        assert entry['breathing_rate_timestamp'] == data_list[i]['breathing_rate_timestamp']
        assert entry['breathing_rate_value'] == data_list[i]['breathing_rate_value']

    print(response.json())
    print(response)
    # assert False
    
    # end of query blood pressure by time stamp
    
    # clean up
    
    query = """
    mutation deleteBreathingRate($DeleteBreathingRateInput:DeleteBreathingRateInput!){
        deleteBreathingRate(input:$DeleteBreathingRateInput){
                user_id
                breathing_rate_timestamp
                breathing_rate_value
            }
        }
        """  
      
   
    # print (data_list)
    # assert False
    for x in data_list:
        variable = """
            {
                "DeleteBreathingRateInput": {
                    "breathing_rate_timestamp": """+ str(x['breathing_rate_timestamp']) +"""
                }
            }
            """
        headers = {'x-api-key': login['X_API_KEY'],
                    'Authorization': login['Authorization'],
                    'Content-Type': "application/graphql"}

        response = requests.post(login['APPSYNC_API_ENDPOINT_URL'], json={
                'query': query, 'variables': variable}, headers=headers)
            
        print('attempte to delete: ', x['breathing_rate_timestamp'])
        assert response.status_code == 200
        