import requests
from requests_aws4auth import AWS4Auth
import json
import datetime

def test_16_listBreathingRates_pass(login):
    query = """
    query listBreathingRates{
        listBreathingRates{
            items{
            user_id
            breathing_rate_timestamp
            breathing_rate_value
            }
        }
    }


"""
    

    headers = {'x-api-key': login['X_API_KEY'],
            'Authorization': login['Authorization'],
            'Content-Type': "application/graphql"}

    response = requests.post(login['APPSYNC_API_ENDPOINT_URL'], json={
    'query': query}, headers=headers)
    
    data = response.json()['data']
   
    print(response.json())
    print(response)
    # assert False