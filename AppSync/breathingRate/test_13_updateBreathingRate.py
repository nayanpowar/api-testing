import requests
from requests_aws4auth import AWS4Auth
import json

def test_13_updateBreathingRate_pass(login, breathing_rate):
    query = """
      mutation updateBreathingRate($UpdateBreathingRateInput:UpdateBreathingRateInput!){
        updateBreathingRate(input:$UpdateBreathingRateInput){
            user_id
            breathing_rate_timestamp
            breathing_rate_value
        }
    }
    
    """
    timestamp =  breathing_rate['breathing_rate_timestamp']
    variable = """
    {
        "UpdateBreathingRateInput": {
            "breathing_rate_timestamp": """+ str(timestamp) +""",
            "breathing_rate_value": 145
        }

    }

    """

    headers = {'x-api-key': login['X_API_KEY'],
            'Authorization': login['Authorization'],
            'Content-Type': "application/graphql"}

    response = requests.post(login['APPSYNC_API_ENDPOINT_URL'], json={
    'query': query, 'variables': variable}, headers=headers)
    print(response.json())
    print(response)

    data = response.json()['data']
    assert data['updateBreathingRate']['breathing_rate_timestamp'] == breathing_rate['breathing_rate_timestamp'] 
    assert data['updateBreathingRate']['breathing_rate_value'] == 145
       
    query = """
    query getBreathingRate($breathing_rate_timestamp: Float!) {
        getBreathingRate(breathing_rate_timestamp: $breathing_rate_timestamp) {
            user_id
            breathing_rate_timestamp
            breathing_rate_value
            
        }
    }
    
    """
    variable = """
    {
        "breathing_rate_timestamp":  """+str(timestamp)+"""
    }
    """

    response = requests.post(login['APPSYNC_API_ENDPOINT_URL'], json={
    'query': query, 'variables': variable}, headers=headers)
    
    data = response.json()['data']
    assert data['getBreathingRate']['breathing_rate_timestamp'] == breathing_rate['breathing_rate_timestamp'] 
    assert data['getBreathingRate']['breathing_rate_value'] == 145
    print(response.json())
    print(response)
    
    