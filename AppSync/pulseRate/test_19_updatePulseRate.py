import requests
from requests_aws4auth import AWS4Auth
import json

def test_19_updatePulseRate_pass(login, pulse_rate):
    query = """
      mutation updatePulseRate($UpdatePulseRateInput:UpdatePulseRateInput!){
        updatePulseRate(input:$UpdatePulseRateInput){
            user_id
            pulse_rate_timestamp
            pulse_rate_value
        }
    }
    
    """
    timestamp =  pulse_rate['pulse_rate_timestamp']
    variable = """
    {
        "UpdatePulseRateInput": {
            "pulse_rate_timestamp": """+str(timestamp)+""",
            "pulse_rate_value": 145
        }
    }

    """

    headers = {'x-api-key': login['X_API_KEY'],
            'Authorization': login['Authorization'],
            'Content-Type': "application/graphql"}

    response = requests.post(login['APPSYNC_API_ENDPOINT_URL'], json={
    'query': query, 'variables': variable}, headers=headers)
    print(response.json())
    print(response)

    data = response.json()['data']
    assert data['updatePulseRate']['pulse_rate_timestamp'] == pulse_rate['pulse_rate_timestamp'] 
    assert data['updatePulseRate']['pulse_rate_value'] == 145
       
    query = """
    query getPulseRate($pulse_rate_timestamp: Float!) {
        getPulseRate(pulse_rate_timestamp: $pulse_rate_timestamp) {
          user_id
          pulse_rate_timestamp
          pulse_rate_value
        }
    }
    
    """
    variable = """
    {
        "pulse_rate_timestamp":  """+str(timestamp)+"""
    }
    """

    response = requests.post(login['APPSYNC_API_ENDPOINT_URL'], json={
    'query': query, 'variables': variable}, headers=headers)
    
    data = response.json()['data']
    assert data['getPulseRate']['pulse_rate_timestamp'] == pulse_rate['pulse_rate_timestamp'] 
    assert data['getPulseRate']['pulse_rate_value'] == 145
    print(response.json())
    print(response)
    
    