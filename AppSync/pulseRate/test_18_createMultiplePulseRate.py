import requests
from requests_aws4auth import AWS4Auth
import json
import datetime

hr_list = [
    {'pulse_rate_timestamp': 0.0, 'pulse_rate_value':143 },
    {'pulse_rate_timestamp': 0.0, 'pulse_rate_value':144 },
    {'pulse_rate_timestamp': 0.0, 'pulse_rate_value':145 },
    {'pulse_rate_timestamp': 0.0, 'pulse_rate_value':146 },
]


def test_18_createMultiplePulseRate_pass(login):
    # put for loop
    for hr in hr_list:

        query = """
        
        mutation createPulseRate($CreatePulseRateInput:CreatePulseRateInput!){
            createPulseRate(input:$CreatePulseRateInput){
                user_id
                pulse_rate_timestamp
                pulse_rate_value
            }
        }


        """
        ct = datetime.datetime.utcnow()
        timestamp = ct.timestamp()
        hr['pulse_rate_timestamp'] = timestamp

        variable = """
        {
        "CreatePulseRateInput": {
                "pulse_rate_timestamp": """+str(timestamp)+""",
                "pulse_rate_value": """+str(hr['pulse_rate_value'])+"""
        }
        }
        """

        headers = {'x-api-key': login['X_API_KEY'],
                   'Authorization': login['Authorization'],
                   'Content-Type': "application/graphql"}

        response = requests.post(login['APPSYNC_API_ENDPOINT_URL'], json={
            'query': query, 'variables': variable}, headers=headers)

        print(response.json())
        print(response)
        data = response.json()['data']
        assert data['createPulseRate']['pulse_rate_timestamp'] == hr['pulse_rate_timestamp']
        assert data['createPulseRate']['pulse_rate_value'] == hr['pulse_rate_value']
        

    # clean up
    for hr in hr_list:
        query = """
        mutation deletePulseRate($DeletePulseRateInput:DeletePulseRateInput!){
        deletePulseRate(input:$DeletePulseRateInput){
                user_id
                pulse_rate_timestamp
                pulse_rate_value
            }
        }
        """
        variable = """
        {
            "DeleteBloodPressureInput": {
                "pulse_rate_timestamp": """+ str(hr['pulse_rate_timestamp']) +"""
            }
        }
        """
        
        headers = {'x-api-key': login['X_API_KEY'],
                'Authorization': login['Authorization'],
                'Content-Type': "application/graphql"}

        response = requests.post(login['APPSYNC_API_ENDPOINT_URL'], json={
            'query': query, 'variables': variable}, headers=headers)