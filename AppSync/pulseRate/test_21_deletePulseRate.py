import requests
from requests_aws4auth import AWS4Auth
import json
import datetime

def test_21_createPulseRate_pass(login):
    query = """
    mutation deletePulseRate($DeletePulseRateInput:DeletePulseRateInput!){
        deletePulseRate(input:$DeletePulseRateInput){
            user_id
            pulse_rate_timestamp
            pulse_rate_value
        }   
    }

    """
    ct = datetime.datetime.now()
    timestamp = ct.timestamp()

    variable = """
   {
    "DeletePulseRateInput": {
            "pulse_rate_timestamp": """+str(timestamp)+"""
        }
    }

    """
    headers = {'x-api-key': login['X_API_KEY'],
            'Authorization': login['Authorization'],
            'Content-Type': "application/graphql"}

    response = requests.post(login['APPSYNC_API_ENDPOINT_URL'], json={
        'query': query, 'variables': variable}, headers=headers)