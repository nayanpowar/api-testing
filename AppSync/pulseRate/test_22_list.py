import requests
from requests_aws4auth import AWS4Auth
import json
import datetime

def test_22_createPulseRate_pass(login):
    query = """
    query listPulseRates{
        listPulseRates{
            items{
            user_id
            pulse_rate_timestamp
            pulse_rate_value
            }
        }
    }


"""
    

    headers = {'x-api-key': login['X_API_KEY'],
            'Authorization': login['Authorization'],
            'Content-Type': "application/graphql"}

    response = requests.post(login['APPSYNC_API_ENDPOINT_URL'], json={
    'query': query}, headers=headers)
    
    data = response.json()['data']
   
    print(response.json())
    print(response)
    # assert False