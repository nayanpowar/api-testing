import requests
import json

url = "https://blrugreno1.execute-api.us-west-2.amazonaws.com/QAStage/api/users/updateuserattributes"

data = {
    "weight": "185",
    "weight_unit": "lbs"
}


def test_03_update_user_attributes_pass(login):
    headers = {
        'Content-Type': 'application/json',
        'Authorization': login['Authorization'],
        'AccessToken':   login['AccessToken']
    }

    response = requests.request(
        "POST", url, data=json.dumps(data), headers=headers)
    assert response.status_code == 200


# No token
def test_04_update_user_attributes_fail_1(login):
    headers = {
        'Content-Type': 'application/json',
        'Authorization': login['Authorization'],
    }

    response = requests.request(
        "POST", url, data=json.dumps(data), headers=headers)
    assert response.status_code == 400

# Expired token
def test_05_update_user_attributes_fail_2(login):
    headers = {
        'Content-Type': 'application/json',
        'Authorization': login['Authorization'],
        'AccessToken':   'slkjfsdl;kfsd;lfsdl;afs;daf'
    }

    response = requests.request(
        "POST", url, data=json.dumps(data), headers=headers)
    assert response.status_code != 200
