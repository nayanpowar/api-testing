from _pytest.python import Module
import requests
import json
import pytest

url = "https://blrugreno1.execute-api.us-west-2.amazonaws.com/QAStage/api/users/login"
   
authorization = "a64bccc5-1648-46ae-ad78-b0f890f1d6c1"

def test_01_login_pass():
    data = {
        'username': 'movano.test@gmail.com',
        'password': 'Csswakad@8782',
        'device_id': '1234'
    } 
    headers = {
        'Content-Type': 'application/json',
        'Authorization':authorization
    }
    response = requests.request(
        "POST", url, data=json.dumps(data), headers=headers)
    assert response.status_code == 200
    print ('response.text:',response.text)


@pytest.mark.parametrize("data", [{
        'username': 'wrong',
        'password': 'Csswakad@8782',
        'device_id': '123456'
        },
        {
            'username': 'movano.test@gmail.com',
            'password': 'Csswakad@8782--',
            'device_id': '12345'
        },
    ])
def test_02_login_fail(data):
    headers = {
        'Content-Type': 'application/json',
        'Authorization': 'a64bccc5-1648-46ae-ad78-b0f890f1d6c1'
    }
    response = requests.request(
        "POST", url, data=json.dumps(data), headers=headers)
    assert response.status_code == 400