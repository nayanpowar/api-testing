import requests
import json


url = 'https://blrugreno1.execute-api.us-west-2.amazonaws.com/QAStage/api/users/validateregistrationcode'
authorization = 'a64bccc5-1648-46ae-ad78-b0f890f1d6c1'

def test_13_validationregistercode_pass():
    data = {
        'username':"movano.test104@gmail.com",
        'code':"503768"
    }
    headers = {
        'Content-Type': 'application/json',
        'Authorization': authorization
    }
    response = requests.request(
        "POST", url, data=json.dumps(data), headers=headers)
    
    assert response.status_code == 200