import requests
import json
import pytest

url = "https://blrugreno1.execute-api.us-west-2.amazonaws.com/QAStage/api/users/changepassword"


def test_09_change_password_pass(login):
    data = {
        "old_password": "Csswakad@8782",
        "new_password": "Csswakad@8782"
    }

    headers = {
        'Content-Type': 'application/json',
        'Authorization': login['Authorization'],
        'AccessToken':   login['AccessToken']
    }

    response = requests.request(
        "POST", url, data=json.dumps(data), headers=headers)
    print('response from change password is:', response)
    print('response.text:', response.text)
    data = response.json()['data']  
    assert response.status_code == 200


# Wrong password
def test_10_change_password_fail_1(login):
    data = {
        "old_password": "wrong",
        "new_password": "Csswakad@8782"
    }
    headers = {
        'Content-Type': 'application/json',
        'Authorization': login['Authorization'],
        'AccessToken':   login['AccessToken']
    }

    response = requests.request(
        "POST", url, data=json.dumps(data), headers=headers)
    assert response.status_code == 400

# Expired token


def test_11_change_password_fail_2(login):
    headers = {
        'Content-Type': 'application/json',
        'Authorization': login['Authorization'],
        'AccessToken':   login['AccessToken']
    }
    data = {
        "old_password": "Vik@1234567",
        "new_password": "Vik@12345"
    }

    response = requests.request(
        "POST", url, data=json.dumps(data), headers=headers)
    assert response.status_code != 200