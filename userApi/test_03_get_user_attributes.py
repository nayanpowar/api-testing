import requests
import json
import pytest

url = "https://blrugreno1.execute-api.us-west-2.amazonaws.com/QAStage/api/users/getuserattributes"

data = {
    "weight": "185",
    "weight_unit": "lbs",
}

@pytest.fixture(scope="module")
def user_attributes(login):
    url = "https://blrugreno1.execute-api.us-west-2.amazonaws.com/QAStage/api/users/updateuserattributes"
    headers = {
        'Content-Type': 'application/json',
        'Authorization': login['Authorization'],
        'AccessToken':   login['AccessToken']
    }

    response = requests.request(
        "POST", url, data=json.dumps(data), headers=headers)
    assert response.status_code == 200
    return data


def test_06_get_user_attributes(login, user_attributes):
    headers = {
        'Content-Type': 'application/json',
        'Authorization': login['Authorization'],
        'AccessToken':   login['AccessToken']
    }

    response = requests.request(
        "GET", url, data=json.dumps(data), headers=headers)
    assert response.status_code == 200
    jsonResponse = response.json()
    assert jsonResponse['data']['weight'] == "185"
    assert jsonResponse['data']["weight_unit"] == "lbs"    

# No token
def test_07_get_user_attributes_fail_1(login, user_attributes):
    headers = {
        'Content-Type': 'application/json',
        'Authorization': login['Authorization'],
    }

    response = requests.request(
        "POST", url, data=json.dumps(data), headers=headers)
    assert response.status_code != 200

## Expired token
def test_08_get_user_attributes_fail_2(login):
    headers = {
        'Content-Type': 'application/json',
        'Authorization': login['Authorization'],
        'AccessToken':   'slkjfsdl;kfsd;lfsdl;afs;daf'
    }

    response = requests.request(
        "POST", url, data=json.dumps(data), headers=headers)
    assert response.status_code != 200
